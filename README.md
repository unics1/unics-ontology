# UNiCS - The University Analytics Ontology

The UNiCS ontology has been developed since the year 2017 by the research division of SIRIS Academic ([unics.cloud](http://unics.cloud/)) to support the data integration and data access in the Research and Innovation domain.

In its current version, UNiCS includes concepts and relationships that cover the ambits of:
* higher education (university personnel, student enrollement, gender distribution, disciplinary areas, etc.)
* competitive funded projects at international, national, regional levels
* patents
* collaborative research and innovation networks (of private and public organisations)
* clinical trials
* financial profiles of research and academic organisations
* university rankings, evaluation metrics, and key performance indicators
* contextual information and geo-based statistical data.


UNiCS is the core component of several running business intelligence platforms and decision making support systems.
In fact, the actual users of the UNiCS ontology range from policy makers in regional governments, to management boards of academic institutions, and private foundations devoted to research and innovation funding. For them, UNiCS has become the prerequisite for the design and implementation of customised data exploration services, which provide factual evidence to their policy and decision making activities.

This repository https://gitlab.com/unics1/unics-ontology includes:
* the source OWL code of the UNiCS T-Box
* a list of UML-like schema documenting the modules in which the ontology is conceptually organised, and
* related scientific publications and other dissemination material.

Main contributors: [Alessandro Mosca](http://www.inf.unibz.it/~almosca/) (KRDB Research Centre for Knowledge and Data, Free University of Bozen-Bolzano, Italy), [Guillem Rull](https://www.sirisacademic.com/wb/team/guillem-rull-fort/) (SIRIS Academic SL, Spain), [Fernando Roda](https://www.cifasis-conicet.gov.ar/personal/93) (Centro Internacional Franco Argentino de Ciencias de la Información y de Sistemas, CONICET-UNR, Argentina).